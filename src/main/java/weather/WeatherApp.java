package weather;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import weather.Weather;

public class WeatherApp {
	// HTTP Client:
	// https://hc.apache.org/httpcomponents-client-4.5.x/quickstart.html
	public static void main(String[] args) throws ClientProtocolException, IOException {

		System.out.println("==================\nWeather application\n==================");

		final String APP_ID = "ddc3b7a55919d8f28d68217135b92a07";
		final String ADDRESS_API = "http://api.openweathermap.org/data/2.5/forecast";

		CloseableHttpClient httpclient = HttpClients.createDefault();
		
		final String ADDRESS = new StringBuilder(ADDRESS_API)
				.append("?")
				.append("q=")
				.append("Lodz,pl")
				.append("&")
				.append("appid=")
				.append(APP_ID)
				.toString();
		
		HttpGet httpGet = new HttpGet(ADDRESS);
		
		String json = null;
		try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
		   System.out.println(response.getStatusLine());
		    HttpEntity entity = response.getEntity();
		    json = EntityUtils.toString(entity, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		Weather pogoda = null;

		Gson gson = new Gson();
			pogoda = gson.fromJson(json, Weather.class);

		if (json == null || json.isEmpty()) {
			System.err.println("Server response is empty");
		}

		else {
			System.out.println(pogoda.getDescription() + " " + pogoda.getMain());
		}
	}
}